﻿# Introdução

O seguinte projeto visa demonstrar a trajetória percorrida por uma bola que será solta dentro de uma caixa em 2d. Em nível de programação, ele demonstra como ocorre a interação entre classes, o conceito de funções virtuais e a sobrecarga de funções. Através do programa [astah](http://astah.net/editions/community) criaremos um diagrama das classes enolvidas.

# Objetivo

O objetivo dessa etapa é entender os conceitos utilizados, de forma aplicada, além de criar construtores que podem receber diversos tipos de argumentos. Deve-se notar que a partir dos métodos criados pode-se modificar a velocidade, peso, altura inicial ou número de iterações (quantas coordenadas serão geradas) da bola.

# Requisitos

 - `Bibliotecas:`
  
    **#include< iostream >**

 - `C++ / Compilador:`
 
    versão usada: **gcc version 5.4.0**

# Linha de Comando

> Necessita usar-se o arquivo Makefile presente no repositório.
>
> $make test-ball
>
> $./test-ball

# Arquivos  

> - `simulation.h`
>
>    Protótipo das funções virtuais que serão implementadas no arquivo **ball.cpp**
>
>  - `ball.h`
>
>  Atributos e protótipos das funções que serão implementadas no arquivo **ball.cpp**
>
> - `ball.cpp`
>
>  Implementação das funções dos arquivos de **ball.h** e **simulation.h**
>
>- `test-ball.cpp`
>
>   Arquivo onde será implementada a função main, que fornecerá a saída dos dados referentes ao projeto


# Saída

Como saída, o programa gera um conjunto de `10000` (esse número pode ser alterado) `coordenadas` (x y) que representam a posição da bola naquele instante.

# Diagrama


![Diagrama das classes usadas](diagrama.png)

# Gráfico

 Usando a ferramenta [GnuPlot](http://www.gnuplot.info) podemos criar o gráfico à partir de um arquivo de coordenadas (x y) com os seguintes comandos :
 
 > set title 'titulo do gráfico' (**define o título do gráfico**)
 
 > set key above  (**coloca o título acima do gráfico**)
 
 > set xlabel 'x' (**coloca a informação passada entre aspas abaixo do eixo x do gráfico**)
 
 > set ylabel 'y' (**coloca a informação passada entre aspas ao lado do eixo y do gráfico**)
 
 > plot 'caminho do arquivo' title 'informação' with lines (**_o comando `with lines` colocado no final da linha de comando serve para fazer a leitura do arquivo linha a linha, até que o arquivo termine_**)
 > `EXEMPLO:`
 > **plot 'C:\Users\admin\Desktop\nomedoarquivo.txt' title 'Vendas de Março' with lines**
 
 > `NOTA: Todas as aspas utilizadas nos comandos acima são necessárias.`
 
 Ao plotar-se as coordenadas em um gráfico 2d, temos a imagem ilustrada abaixo. Como pode-se observar, a imagem mostra a trajetória da bola, que sempre continua pulando sem perder energia, pois não há nenhuma força além da cinética agindo sobre ela.
 
 ![Gráfico da Trajetória](grafico.png)
