﻿# Introdução

Em sua parte final, esse projeto visa demonstrar a trajetória graficamente representada dos conjuntos de coordenadas dos projetos criados anteriormente, **Bouncing Ball** e **SpringMass**. Com a utilização de uma ferramenta gráfica, chamada OpenGL, será possivel a criação de alguns tipos de objetos gráficos como strings, linhas e circulos. O sistema (massa-mola ou apenas massa) será solto dentro de uma caixa em 2d, onde ficará pingando até que perca toda a energia e entre em repouso, ou no caso do projeto **Bouncing Ball**, continue pingando sem perca de energia. Através do programa [astah](http://astah.net/editions/community) criaremos um diagrama das classes enolvidas.

# Requisitos
 
 - `Programas:`
 
    **OpenGL**
    
    **Glut** 
 
 - `Bibliotecas:`

    **#include < iostream >**
    
    **#include < sstream >**
    
    **#include < iomanip >**
    
    **#include < cmath >**
    
    **#include < vector >**
    
     >**Bibliotecas do OpenGL e GLUT**
     >
     > #if defined(__APPLE__)
     >
     > #include <OpenGL/OpenGL.h>
     >
     > #include <GLUT/glut.h>
     >
     > #elif defined(_WIN32)
     >
     > #include <GLUT/glut.h>
     >
     > #else
     >
     > #include <GL/gl.h>
     >
     > #include <GL/glu.h>
     >
     > #include <GL/glut.h>
     >
     > #endif


 - `C++ / Compilador:`
 
    versão usada: **gcc version 5.4.0**

# Linha de Comando

> sudo apt-get install freeglut3 freeglut3-dev  //essa linha instalará as bibliotecas necessárias ao OpenGL
>
> Necessita-se usar o arquivo Makefile presente no repositório.
>
> $make test-ball-graphics
>
> $./test-ball-graphics
>
> $make test-springmass-graphics
>
> $./test-springmass-graphics

# Arquivos  

> - `simulation.h`
>
>    Protótipo das funções virtuais que serão implementadas nas classes filhas
>
> - `springmass.h`
>
>  Atributos e protótipos das funções que serão implementadas no arquivo **springmass.cpp**
>
> - `ball.h`
>
>  Atributos e protótipos das funções que serão implementadas no arquivo **ball.cpp**
>
> - `graphics.h`
>
>  Atributos e protótipos das funções que serão implementadas no arquivo **graphics.cpp**
>
> - `test-springmass-graphics.cpp`
>
>   Arquivo onde será implementada a função main, que fornecerá a saída gráfica do projeto **SpringMass**
>
> - `test-ball-graphics.cpp`
>
>   Arquivo onde será implementada a função main, que fornecerá a saída gráfica do projeto **Bouncing Ball**


# Diagrama


![Diagrama das classes usadas](DiagGra.png)

# Programa em andamento

![exe](art.jpeg)

