/** file: test-srpingmass.cpp
 ** brief: Tests the spring mass simulation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"
#include <iostream>

int main(int argc, char** argv)
{
  SpringMass springmass ;

  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.09 ;
  const double stiffness = 0.55;
  const double damping = 0.5;

  Mass m1(Vector2(0.5,-0.7), Vector2(5,5), mass, radius) ;
  Mass m2(Vector2(-0.6,0.4), Vector2(4,3), mass, radius) ;
  
  //-----------TASK 13--------------
  
  springmass.addMass(&m1);
  springmass.addMass(&m2);
  springmass.addSpring(0, 1, naturalLength, stiffness, damping);
  
  const double dt = 1.0/30 ;
  for (int i = 0 ; i < 500 ; ++i) {
    springmass.step(dt) ;
    springmass.display() ;
    std::cout << "\n";
  }

  return 0 ;
}
