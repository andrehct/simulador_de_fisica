﻿# Introdução

O seguinte projeto visa demonstrar a trajetória percorrida por duas massas (que serão tratadas como esferas) ligadas entre sí por uma mola. O sistema (massa-mola) será solto dentro de uma caixa em 2d, onde ficará pingando até que perca toda a energia e entre em repouso. Em nível de programação, ele demonstra como ocorre a interação entre classes, o conceito de funções virtuais e a sobrecarga de funções. Através do programa [astah](http://astah.net/editions/community) criaremos um diagrama das classes enolvidas.

# Objetivo

O objetivo dessa etapa é entender e aplicar os conceitos físicos ao projeto ligando massas nos extremos de uma mola. À partir dessa compreensão serão criadas algumas funções para manipular o sistema. Deve-se notar que a partir dos métodos criados pode-se adicionar massas e molas (podendo por exemplo mudar o peso das massas ou alterar a elasticidade das molas, individualmente) ao sistema, ou mudar o número de iterações (quantas coordenadas serão geradas). Não será permitido conectar uma massa em apenas uma mola, ou seja, toda mola tem obrigatoriamente que estar conectada à duas massas.

# Requisitos

 - `Bibliotecas:`

    **#include< iostream >**
    **#include< vector >**

 - `C++ / Compilador:`
 
    versão usada: **gcc version 5.4.0**

# Linha de Comando

> Necessita-se usar o arquivo Makefile presente no repositório.
>
> $make test-springmass
>
> $./test-springmass

# Arquivos  

> - `simulation.h`
>
>    Protótipo das funções virtuais que serão implementadas nas classes filhas
>
>  - `springmass.h`
>
>  Atributos e protótipos das funções que serão implementadas no arquivo **springmass.cpp**
>
> - `springmass.cpp`
>
>  Implementação das funções dos arquivos **springmass.h** e **simulation.h**
>
>- `test-springmass.cpp`
>
>   Arquivo onde será implementada a função main, que fornecerá a saída dos dados referentes ao projeto


# Saída

Como saída, para plotagem, o programa gera um conjunto de `1000` (esse número pode ser alterado) `coordenadas` x y que representam a posição da massa naquele instante. Todas as massas contidas no sistema terão suas coordenadas impressas antes da proxima atualização de posição.

Em um sistema com 2 massas (A, B). A saída gerada será como a seguinte:

   > posicao_x_massaA posicao_y_massaA posicao_x_massaB posicao_y_massaB
   >
   > posicao_x_massaA posicao_y_massaA posicao_x_massaB posicao_y_massaB
   >
   > ...
   
Em sua forma original, a saída tem mais dois argumentos, que são a energia total do sistema naquele momento e o comprimento da mola.

   > posicao_x_massaA posicao_y_massaA posicao_x_massaB posicao_y_massaB comprimento-da-mola
   >
   > energia-do-sistema
   >
   > posicao_x_massaA posicao_y_massaA posicao_x_massaB posicao_y_massaB comprimento-da-mola
   >
   > energia-do-sistema
   >
   > ...

# Diagrama


![Diagrama das classes usadas](springmass.png)

# Gráfico

Para plotar o gráfico, faça como dito na parte `linha de comando` citado anteriormente. Note que no arquivo springmass.cpp o método `void SpringMass::display() (linha 183)` deve ter um pequeno trecho comentado. Para gerar um arquivo de saída digite o seguinte comando no terminal: 

   >$./test-springmass > x.txt

Usando a ferramenta [Octave](https://www.gnu.org/software/octave/) podemos criar o gráfico à partir do arquivo de coordenadas gerado acima.
Deve-se colocar o arquivo de saída (x.txt) na mesma pasta do arquivo plot_springmass.m. Em seguida, digite o seguinte comando no terminal:

  >$octave plot_ball.m

Será gerado um gráfico ilustando a posição das massas, ligadas por uma linha que representa a mola do sistema.
